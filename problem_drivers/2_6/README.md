# 项目2.6: 在5.2节的broker.c程序中添加循环，以便用户可以输入多笔交易并且程序可以计算每次的佣金。程序在用户输入的交易额为0时终止。

```
Enter value of trade：30000
Commission：$166.00
Enter value of trade：20000
Commission：$144.00
Enter value of trade：0
```