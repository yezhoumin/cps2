#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#define MAP_LENGTH 10
#define DIRECTION 4
// up 0
// right 1
// down 2
// left 3
const int dx[] = {-1, 0, 1, 0};
const int dy[] = {0 ,1, 0, -1};
char map[MAP_LENGTH][MAP_LENGTH];

void init()
{
    for (int i = 0; i < MAP_LENGTH; ++i)
        for (int j = 0; j < MAP_LENGTH; ++j)
            map[i][j] = '.';
    srand((unsigned int)time(NULL));
}

int get_direction()
{
    return rand() % DIRECTION;
}

int in_map(int x , int y)
{
    return 0 <= x && x < MAP_LENGTH && 0 <= y && y < MAP_LENGTH;
}

int can_move(int x, int y)
{
    int ret = 0;
    for(int d = 0; !ret && d < 4; ++d)
    {
        if(in_map(x + dx[d], y + dy[d]) && map[x + dx[d]][y + dy[d]] == '.')
        {
            ret = 1;
        }
    }
    return ret;
}

void random_move(int *x, int *y)
{
    int movable[] = {0, 0, 0, 0};
    int option = 0;
    //check
    for(int d = 0; d < 4; ++d)
        if(in_map(*x + dx[d], *y + dy[d]) && map[*x + dx[d]][*y + dy[d]] == '.')
        {
            movable[d] = 1;
            ++option;
        }
    //option can't be 0
    int choice_idx = rand() % option;
    for(int d = 0; d < 4; ++d)
        if(movable[d] == 1)
        {
            if(choice_idx == 0)
            {
                *x = *x + dx[d];
                *y = *y + dy[d];
                return;
            }
            else
            {
                --choice_idx;
            }
        }
}

void walk()
{
    char cur_char = 'A';
    int x = 0, y = 0;
    map[x][y] = cur_char++;
    while(cur_char <= 'Z')
    {
        if(!can_move(x, y))return;
        random_move(&x, &y);
        map[x][y] = cur_char++;
    }
}

void print()
{
    for (int i = 0; i < MAP_LENGTH; ++i)
    {
        for (int j = 0; j < MAP_LENGTH; ++j)
            printf("%c", map[i][j]);
        printf("\n");
    }
}

int main()
{
    init();
    walk();
    print();

    return 0;
}
