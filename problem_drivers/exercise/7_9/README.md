### 7-9：

  编写程序要求用户输入 12 小时制的时间，然后用 24 小时制显示该时间：

		Enter a 12-hour time: 9:11 PM
		Equivalent 24-hour time: 21:11

参考编程题 8 中关于输入格式的描述。
