﻿### 第 8 章编程题 14

编写程序颠倒句子中单词的顺序：

     Enter a sentence: you can cage a swallow can't you?
     Reversal of sentence: you can't swallow a cage can you?
**提示**：用循环逐个读取字符，然后将它们存储在一个一维字符数组中。当遇到句号、问号或者感叹
号（称为 “终止字符” ）时，终止循环并把终止字符存储在一个 char 类型变量中。然后再用一个循环
反向搜索数组，找到最后一个单词的起始位置。显示最后一个单词，然后反向搜索倒数第二个单词。
重复这一过程，直至到达数组的起始位置。最后显示出终止字符。




