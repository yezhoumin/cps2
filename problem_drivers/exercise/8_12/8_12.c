#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#define BUFFERSIZE 40

int main(int argc, char *argv)
{
	const int value[] = 
	{1, 3, 3, 2, 1, 4, 2, 4, 1, 8, 5,
	 1, 3, 1, 1, 3, 10, 1, 1, 1, 1, 4,
	 4, 8, 4, 10};

    char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length = 0;
	int result = 0;
	char temp = 0;

	printf("Enter a word: ");
	fgets(buffer, sizeof(buffer), stdin);
	length = strlen(buffer);
	for (int i = 0; i < length - 1; i++)
	{
		temp = buffer[i];
		if (temp >= 'a' && temp <= 'z')
			temp = toupper(buffer[i]);
		temp = temp - 65;
        result += value[(int)temp];
        temp = 0;
	}

	printf("Scrabble value: %d\n", result);

}