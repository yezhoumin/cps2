﻿### 第 8 章编程题 16

编程测试两个单词是否为变位词（相同字母的重新排列）：

     Enter first word: smartest
     Enter second word: mattress
     The words are anagrams
     
     Enter first word: dumbest
     Enter second word: stumble
     The words are not anagrams
用一个循环逐个字符地读取第一个单词，用一个 26 元的整数数组记录每个字母的出现次数。（例如，
读取单词 smartest 之后，数组包含的值为 10001000000010000122000000，表明 smartest 包含一个
a、一个 e、一个 m、一个 r、两个 s 和两个 t 。）用另一个循环去读取第二个单词，这次每读取一个字
母就把相应数组元素的值减 1 。两个循环都该忽略不是字母的那些字符，并且不区分大小写。第二个单
词读取完毕后，再用一个循环来检查数组元素是否全为 0 。如果全是 0 ，那么这两个单词就是变位词。
**提示**：可以使用 <ctype.h> 中的函数，如 isalpha 和 tolower。



