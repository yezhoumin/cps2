## 第二十二章

18. 编写程序从文本文件中读取整数，文本文件的名字由命令行参数给出，文件的每一行可以包含任意数量的整数（也可以没有，）中间用一个或多个空格隔开，程序显示文件中最大的数，最小的数以及中值（整数有序情况下最接近中间的那个数）如果文件包含偶数个整数，中间会有两个整数，程序显示它们的均值（向下取整）可以假定文件包含的整数个数不超过 10000，提示，把整数存储在数组中并对其排序。