# 项目2.7: 编写程序，提示用户输入一个数 n，然后显示 出 1~n 的所有偶数平方值。例如，如果用户输入 100，那么程序应该显 示出下列内容：
```
4
16
36
64
100
```