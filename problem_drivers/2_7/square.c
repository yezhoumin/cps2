
#include <stdio.h>

int main()
{
    int n;

    scanf("%d" , &n);
    for(int i = 1; i * i <= n; i++)
    {
        if((i&1) == 0)
            printf("%d\n" , i * i);
    }
}