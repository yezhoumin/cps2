# 项目3.5: 已知的最古老的一种加密技术是凯撒加密(得名于 Julius caesar)。该方法把一条消息中的每个字母用字母表中固定距离之后的那个字母来替代。(如果越过了字母Z,会绕回到字母表的起始位置。例如,如果每个字母都用字母表中两个位置之后的字母代替,那么Y就被替换为A,Z就被替换为B。)编写程序用凯撒加密方法对消息进行加密。用户输入待加密的消息和移位计数(字母移动的位置数目):
```
Enter message to be encrypted: Go ahead, make my day.
Enter shift amount (1-25):3
Encrypted message: Jr dkhdg, pdh pb gdb.
```