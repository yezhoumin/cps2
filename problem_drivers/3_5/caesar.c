

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

char buf[100];

int main()
{
    printf("Enter message to be encrypted: ");
    int i = 0;
    char ch;
    while((ch = getchar()) != '\n')
        buf[i++] = ch;
    int n;
    printf("Enter shift amount (1-25):");
    scanf("%d" , &n);
    printf("Encrypted message:");

    for(int i = 0 ; i < strlen(buf); i++)
    {
        if(buf[i] >= 'a' && buf[i] <= 'z')
        {
            buf[i] = ((buf[i] - 'a') + n) % 26 + 'a';
        }
        else if(buf[i] >= 'A' && buf[i] <= 'Z')
        {
            buf[i] = ((buf[i] - 'A') + n) % 26  + 'A';
        }
    }
    printf("%s" , buf);
    return 0;
}