#! /bin/bash
solution=$1
case=$2
if [ ! -e $solution ]
then
    echo "$solution does not exist"
fi
if [ ! -e $case ]
then
    echo "$case does not exist."
    exit
fi
while read line
do
    echo $line | ./$solution
    echo
done < $case
