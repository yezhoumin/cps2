#include <stdio.h>
 
int main(){
    int hour, minute;
    
    	printf("Enter a 24-hour time: ");
        scanf("%d:%d", &hour, &minute);
        if(hour<12){
            printf("Equivalent 12-hour time: %d:%.2dAM\n", hour, minute);
        } else if(hour==12 && minute==0){
            printf("Equivalent 12-hour time: %d:%.2dPM\n", hour, minute);
        } else if(hour==24 && minute==0){
            printf("Equivalent 12-hour time: %d:%.2dAM\n", hour-12, minute);
        } else {
            printf("Equivalent 12-hour time: %d:%.2dPM\n", hour-12, minute);
        }

    return 0;
}
